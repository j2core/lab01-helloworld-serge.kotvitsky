package com.j2core.ice.week01;

import org.apache.log4j.Logger;
/**
 * Class HelloWorld - week01
 *
 * @author Serge Kotvitsky on 5/20/16.
 * @version 1.0
 */
public class HelloWorld {

    private static final Logger logger = Logger.getLogger(HelloWorld.class);

    public static void main(String[] args) {

        System.out.println("Hello World!");
        logger.info("Good luck");

    }
}
